package main

import (
	"chainmaker.org/chainmaker/transfer-tool/chainMaker/chainmaker_1.x/chainmaker"
	"chainmaker.org/chainmaker/transfer-tool/config"
	"chainmaker.org/chainmaker/transfer-tool/loggers"
	"fmt"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestFetchBlockByHeight(t *testing.T) {

	height := uint64(5)

	config.InitConfig("./config_path", config.GetConfigEnv())

	logger := loggers.GetLogger(loggers.MODULE_PARSER)

	fetch, err := chainmaker.NewFetcher(height, nil, "chain1", "", "./testdata/sdk_config.yml", nil, logger)

	require.Nil(t, err)

	block, err := fetch.Fetch.FetchBlockByHeight(height)
	fmt.Println(block)

}
