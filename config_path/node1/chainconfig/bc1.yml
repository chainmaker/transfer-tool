#
# Copyright (C) BABEC. All rights reserved.
# Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

# This file is used to generate genesis block.
# The content should be consistent across all nodes in this chain.

# chain id
chain_id: chain1

# chain maker version
version: v2.2.0_alpha

# chain config sequence
sequence: 0

# The blockchain auth type, shoudle be consistent with auth type in node config (e.g., chainmaker.yml)
# The auth type can be permissionedWithCert, permissionedWithKey, public.
# By default it is permissionedWithCert.
# permissionedWithCert: permissioned blockchain, using x.509 certificate to identify members.
# permissionedWithKey: permissioned blockchain, using public key to identify members.
# public: public blockchain, using public key to identify members.
auth_type: "public"

# Crypto settings
crypto:
  # Hash algorithm, can be SHA256, SHA3_256 and SM3
  hash: SM3

# User contract related settings
contract:
  # If the sql support contract is enabled or not.
  # If it is true, storage.statedb_config.provider in chainmaker.yml should be sql.
  enable_sql_support: false

vm:
  #0:chainmaker, 1:zxl, 2:ethereum(reserved)
  addr_type: 1
  # support vm list
  support_list:
    - "wasmer"
    - "gasm"
    - "evm"
    - "dockergo"

# Block proposing related settings
block:
  # Verify the transaction timestamp or not
  tx_timestamp_verify: true

  # Transaction timeout, in second.
  # if abs(now - tx_timestamp) > tx_timeout, the transaction is invalid.
  tx_timeout: 600

  # Max transaction count in a block.
  block_tx_capacity: 100

  # Max block size, in MB
  block_size: 10

  # The interval of block proposing attempts
  block_interval: 2000

# Core settings
core:
  # Max scheduling time of a block, in second.
  # [0, 60]
  tx_scheduler_timeout: 10

  # Max validating time of a block, in second.
  # [0, 60]
  tx_scheduler_validate_timeout: 10

  # Consensus message compression related settings
  # consensus_turbo_config:
    # If consensus message compression is enabled or not.
    # consensus_message_turbo: true

    # Max retry count of fetching transaction in txpool by txid.
    # retry_time: 500

    # Retry interval of fetching transaction in txpool by txid, in ms.
    # retry_interval: 20

# gas account config
account_config:
  enable_gas: false
  gas_count: 0
  gas_admin_address: "ZXff78ca3b84e3f5f91ff18b45fc1ecfda2d5990db"
  default_gas: 0

# Consensus settings
consensus:
  # Consensus type 1-TBFT,5-DPOS
  type: 1

  # Consensus node list
  nodes:
    - org_id: "public"
      node_id:
        - "QmbCX7QfCfTe1eJYAg3qzpVHAFXvGZoX87y9pJoUkHv1mG"
        - "QmTC3sQrumFwVTCLMjnCWcqENDxEGiFzKHvs7hjP9HSxRN"
        - "QmSfcc57xz72oH25DDeSvFsUesbcpmMgT14ATXNQDi37Nv"
        - "QmTSChfHYLuAmzeWn4zjfrzeVvCpi17JiyWTMrbcXESyRU"
#        - "{org5_peerid}"
#        - "{org6_peerid}"
#        - "{org7_peerid}"

  # We can specify other consensus config here in key-value format.
  ext_config:
    # - key: aa
    #   value: chain01_ext11

# Trust roots is used to specify the organizations' root certificates in permessionedWithCert mode.
# When in permessionedWithKey mode or public mode, it represents the admin users.
trust_roots:
  - org_id: "public"
    root:
      - "../config/node1/admin/admin1/admin1.pem"
      - "../config/node1/admin/admin2/admin2.pem"
      - "../config/node1/admin/admin3/admin3.pem"
      - "../config/node1/admin/admin4/admin4.pem"

