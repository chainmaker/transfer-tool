/**
 * @Author: starxxliu
 * @Date: 2021/11/29 8:32 下午
 */

package main

import (
	"encoding/json"
	"log"

	"chainmaker.org/chainmaker-contract-sdk-docker-go/pb/protogo"
	"chainmaker.org/chainmaker-contract-sdk-docker-go/shim"
	"chainmaker.org/chainmaker-go/pb/protogo/common"
	"github.com/golang/protobuf/proto"
)

const defaultVersion = "v1.0.0"

type TransferContract struct {
}

// KVWrite captures a write (update/delete) operation performed during transaction simulation
//type KVWrite struct {
//	Key      string `protobuf:"bytes,1,opt,name=key,proto3" json:"key,omitempty"`
//	IsDelete bool   `protobuf:"varint,2,opt,name=is_delete,json=isDelete,proto3" json:"is_delete,omitempty"`
//	Value    []byte `protobuf:"bytes,3,opt,name=value,proto3" json:"value,omitempty"`
//}

// 部署合约 需要反向验证在fabric install 实力化交易时要怎么做
func (f *TransferContract) InitContract(stub shim.CMStubInterface) protogo.Response {
	args := stub.GetArgs()

	//2. 如果是nornal调用用户合约类型的交易
	//2.1 得到chainmakerv1的读写集
	origin_rwset := args["origin_rwset"]
	//2.2 得到chainmakerv1的写集map，key是合约名，value，写集kv
	wSet, err := getWSetFromRWSet(origin_rwset)
	if err != nil {
		shim.Error("parseFabricTx happen failed,err :" + err.Error())
	}

	//遍历写集map，得到key是合约名，value，写集kv
	for contractName, wsetkv := range wSet {
		//为啥不用proto，因为合约里面没有这种map类型proto的定义
		//对写集的kv进行序列化
		v, _ := json.Marshal(wsetkv)

		wsetkvmap := map[string][]byte{"tx_wset": v}

		//根据合约名和对应的写集kv的map，进行跨合约调用
		res := stub.CallContract(contractName, defaultVersion, wsetkvmap)
		if res.Status != shim.OK {
			shim.Error("assis call contract " + contractName + " happen err:" + res.Message)
		}
	}

	return shim.Success(nil)
}

// 调用合约  不支持多级的复合主键
func (f *TransferContract) InvokeContract(stub shim.CMStubInterface) protogo.Response {

	// 获取参数，方法名通过参数传递
	args := stub.GetArgs()

	//获取写集kv序列化后的map
	tx_wset := args["tx_wset"]
	if tx_wset == nil {
		return shim.Success(nil)
	}

	wSet := make(map[string][]byte, 0)

	//反序列化得到写集kv
	err := json.Unmarshal(tx_wset, &wSet)
	if err != nil {
		return shim.Error("unmarshal tx_wset err: " + err.Error())
	}

	for k, v := range wSet {
		if len(v) == 0 {
			err := stub.DelStateFromKey(k)
			if err != nil {
				return shim.Error(err.Error())
			}
		} else {
			//根据写集kv，更新chainmaker2.x的写集
			err := stub.PutStateByte(k, "", v)
			if err != nil {
				return shim.Error(err.Error())
			}
		}
	}
	return shim.Success(nil)
}

// 从chainmaker1.x的读写集得到chainmaker的写集，再转换成写集map
func getWSetFromRWSet(rwSetByte []byte) (map[string]map[string][]byte, error) {
	txRWSet := &common.TxRWSet{}
	err := proto.Unmarshal(rwSetByte, txRWSet)
	if err != nil {
		return nil, err
	}

	//将chainmaker1.x的写集放到一个map中，map的key是合约名，map的value是写集的kv，通过wsetmap把写集进行分类、合并
	//因为写集的key不能是不可比较的slice([]byte)，所以把[]byte转换成string
	wsetmap := make(map[string]map[string][]byte)

	for _, wset := range txRWSet.TxWrites {
		wsetmapkv, ok := wsetmap[wset.ContractName]
		//如果wsetmap不存在这个合约就新建一个合约名和并kv赋值
		if !ok {
			wsetmap[wset.ContractName] = map[string][]byte{string(wset.Key): wset.Value}
		} else {
			//如果存在这个合约，就添加这个合约的kv，或者给这个合约已有的key赋新的值
			wsetmapkv[string(wset.Key)] = wset.Value
		}
	}

	return wsetmap, nil
}

func main() {
	err := shim.Start(new(TransferContract))
	if err != nil {
		log.Fatal(err)
	}
}
