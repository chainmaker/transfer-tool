module github.com/transfer-contract/assist

go 1.17

require (
	chainmaker.org/chainmaker-contract-sdk-docker-go v0.0.0-00010101000000-000000000000
	chainmaker.org/chainmaker-go/pb/protogo v1.2.6
	chainmaker.org/chainmaker/common/v2 v2.1.0 // indirect
	chainmaker.org/chainmaker/pb-go/v2 v2.1.0 // indirect
	chainmaker.org/chainmaker/protocol/v2 v2.1.1-0.20211117024857-2641037a7269 // indirect
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/golang/protobuf v1.5.2
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.18.1 // indirect
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d // indirect
	golang.org/x/sys v0.0.0-20211205182925-97ca703d548d // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/genproto v0.0.0-20211208223120-3a66f561d7aa // indirect
	google.golang.org/grpc v1.42.0 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)

require (
	github.com/google/go-cmp v0.5.6 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

replace (
	chainmaker.org/chainmaker-contract-sdk-docker-go => ../../chainmaker-contract-sdk-docker-go
	chainmaker.org/chainmaker-go/common => ../../../chainMaker/chainmakerv1/common
	chainmaker.org/chainmaker-go/localconf => ../../../chainMaker/chainmakerv1/conf/localconf
	chainmaker.org/chainmaker-go/logger => ../../../chainMaker/chainmakerv1/logger
	chainmaker.org/chainmaker-go/pb/protogo => ../../../chainMaker/chainmakerv1/pb/protogo
	chainmaker.org/chainmaker-go/protocol => ../../../chainMaker/chainmakerv1/protocol
	chainmaker.org/chainmaker-go/store => ../../../chainMaker/chainmakerv1/store
	chainmaker.org/chainmaker-go/utils => ../../../chainMaker/chainmakerv1/utils
	github.com/go-kit/kit v0.10.0 => github.com/go-kit/kit v0.8.0
)
