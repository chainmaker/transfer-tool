/**
 * @Author: starxxliu
 * @Date: 2021/11/20 2:20 下午
 */

package store

import (
	"bytes"
	"context"
	"encoding/hex"
	"fmt"
	"strings"
	"sync"
	"sync/atomic"
	"time"
	"unicode/utf8"

	"chainmaker.org/chainmaker/transfer-tool/mulnode"
	"chainmaker.org/chainmaker/transfer-tool/provider"

	commonpb "chainmaker.org/chainmaker/pb-go/v2/common"
	consensuspb "chainmaker.org/chainmaker/pb-go/v2/consensus"
	storePb "chainmaker.org/chainmaker/pb-go/v2/store"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/store/v2"
	"chainmaker.org/chainmaker/transfer-tool/common/msgbus"
	"chainmaker.org/chainmaker/transfer-tool/config"
	"chainmaker.org/chainmaker/transfer-tool/loggers"
)

const (
	PrefixContractInfo     = "Contract:"
	PrefixContractByteCode = "ContractByteCode:"
	maxUnicodeRuneValue    = utf8.MaxRune
)

type StoreImpl struct {
	nodeM       *mulnode.NodeManage
	msgBus      msgbus.MessageBus // message bus, transfer messages with other modules
	store       protocol.BlockchainStore
	log         *loggers.CMLogger
	err         chan error
	blockHeight uint64
	preHash     []byte
	StartFlag   int32
	isMul       bool //是否启动多节点存储
	checker     provider.Checker
	db          provider.TransferDB
	taskId      string
}

func NewStoreImpl(msg msgbus.MessageBus, log *loggers.CMLogger, errChan chan error, ctx context.Context) (*StoreImpl, error) {
	var err error
	var bstore protocol.BlockchainStore
	var manage *mulnode.NodeManage
	if !config.TransferConfig.Chain.IsMul {
		plog := loggers.NewProtocolLog(loggers.GetLogger(loggers.MODULE_CSAVE))
		bstore, err = store.NewFactory().NewStore(config.TransferConfig.Chain.ChainId,
			config.TransferConfig.Storage, plog, nil)
		if err != nil {
			return nil, err
		}
	} else {
		manage = mulnode.NewNodeManage(ctx, config.TransferConfig.Node, log, errChan)
	}
	list, err := GetContractList(bstore)
	if err != nil {
		log.Panicf(err.Error())
	}

	//TODO 增加根据不同来源链的checker的switch
	log.Infof("contract list %+v\n", list)
	return &StoreImpl{
		msgBus:    msg,
		store:     bstore,
		log:       log,
		err:       errChan,
		StartFlag: 0,
		nodeM:     manage,
		isMul:     config.TransferConfig.Chain.IsMul,
	}, nil
}

var OnceLock sync.Mutex

func NewStoreImpl2(msg msgbus.MessageBus, log *loggers.CMLogger, errChan chan error,
	ctx context.Context, checker provider.Checker, db provider.TransferDB, taskId string) (*StoreImpl, error) {
	OnceLock.Lock()
	var err error
	var bstore protocol.BlockchainStore
	var manage *mulnode.NodeManage
	if !config.TransferConfig.Chain.IsMul {

		plog := loggers.NewProtocolLog(loggers.GetLogger(loggers.MODULE_CSAVE))

		bstore, err = store.NewFactory().NewStore(config.TransferConfig.Chain.ChainId,
			config.TransferConfig.Storage, plog, nil)
		if err != nil {
			return nil, err
		}
	} else {
		manage = mulnode.NewNodeManage(ctx, config.TransferConfig.Node, log, errChan)
	}
	list, err := GetContractList(bstore) //打印已经迁移的contracts
	if err != nil {
		log.Panicf(err.Error())
	}

	log.Infof("contract list %+v\n", list)
	return &StoreImpl{
		msgBus:    msg,
		store:     bstore,
		log:       log,
		err:       errChan,
		StartFlag: 0,
		nodeM:     manage,
		isMul:     config.TransferConfig.Chain.IsMul,
		checker:   checker,
		db:        db,
		taskId:    taskId,
	}, nil
}

func (c *StoreImpl) SetStatus(block *commonpb.Block) {
	c.preHash = block.Hash()
	c.blockHeight = block.Header.BlockHeight
	c.log.Infof("preHash [%s]", hex.EncodeToString(c.preHash))
}

// OnQuit called when quit subsribe message from message bus
func (c *StoreImpl) OnQuit() {
	//c.log.Info("on quit")
}

// OnMessage consume a message from message bus
func (c *StoreImpl) OnMessage(message *msgbus.Message) {
	switch message.Topic {
	case msgbus.CommitBlock:
		startT := time.Now()
		if atomic.LoadInt32(&c.StartFlag) != int32(1) {
			c.log.Error("StoreImpl already close")
			return
		}
		SaveProposalB, _ := message.Payload.(*provider.TempBlock)

		if SaveProposalB.Err != nil {
			c.Close(fmt.Errorf("commitBlock err %s", SaveProposalB.Err.Error()))
			return
		}

		if SaveProposalB.Done {
			c.log.Infof("close store module ")
			c.Close(nil)
			return
		}

		proposalB := SaveProposalB.Proposal
		if proposalB.Block.Header.BlockHeight != c.blockHeight+1 || !bytes.Equal(proposalB.Block.Header.PreBlockHash, c.preHash) {
			err := fmt.Errorf("commitBlock block err expect preHash[%s] but is[%s],expect height[%d] but is[%d]", c.preHash,
				proposalB.Block.Header.PreBlockHash, c.blockHeight+1, proposalB.Block.Header.BlockHeight)
			c.log.Error(err)
			c.Close(err)
			return
		}

		names, err := c.checker.CheckBlock(SaveProposalB)
		if err != nil {
			err := fmt.Errorf("CheckBlock[%d] err %s", proposalB.Block.Header.BlockHeight, err)
			c.log.Error(err)
			c.Close(err)
			return
		}

		rwSet := RearrangeRWSet(proposalB.Block, proposalB.TxsRwSet)

		if !c.isMul {
			if err := c.store.PutBlock(proposalB.Block, rwSet); err != nil {
				// if put db error, then panic
				c.log.Error("put block to store err :", err)
				c.Close(err)
				return
				//panic(err)
			}
		} else {
			if err := c.nodeM.PutBlocks(proposalB.Block, rwSet); err != nil {
				// if put db error, then panic
				c.log.Error("ismul model put block to store err :", err)
				c.Close(err)
				return
				//panic(err)
			}
		}

		//clen,_ := proto.Marshal(proposalB.Block)
		//flen,_ := proto.Marshal(SaveProposalB.FabricBlock.Block)
		//
		//c.log.Debugf("store blockInfo[%d],fabric block [%d]byte,chainMaker block[%d]",proposalB.Block.Header.BlockHeight,len(flen),len(clen))

		err = putContractName(c.db, names, proposalB.Block.Header.BlockHeight, c.taskId)
		if err != nil {
			err := fmt.Errorf("CheckBlock err %s", err)
			c.log.Error(err)
			c.Close(err)
			return
		}
		c.log.Debugf("store block height[%d],speed time[%v]", proposalB.Block.Header.BlockHeight,
			time.Since(startT))
		c.preHash = proposalB.Block.Header.BlockHash
		c.blockHeight++
		c.log.Infof("commit height[%d] hash [%s] block success ", c.blockHeight, hex.EncodeToString(c.preHash))
	}
}

// Start initialize core engine
func (c *StoreImpl) Start() {
	atomic.StoreInt32(&c.StartFlag, 1)
	c.msgBus.Register(msgbus.CommitBlock, c) // 我们使用这个topic 来代表TxRequestBatch
	if c.isMul {
		c.nodeM.Start()
	}
}

// Close send err to c.err and close msgBus and store
func (c *StoreImpl) Close(err error) {
	atomic.StoreInt32(&c.StartFlag, 0)
	//c.msgBus.Close()
	time.Sleep(2 * time.Second)
	if !c.isMul {
		errC := c.store.Close()
		if errC != nil {
			c.log.Errorf(err.Error())
		}
	}
	c.err <- err
	OnceLock.Unlock()
}

// PutBlock put block
func (c *StoreImpl) PutBlock(proposal *consensuspb.ProposalBlock) {
	rwSet := RearrangeRWSet(proposal.Block, proposal.TxsRwSet)

	if !c.isMul {
		err := c.store.PutBlock(proposal.Block, rwSet)
		if err != nil {
			c.log.Panic(err)
		}
	} else {
		err := c.nodeM.PutBlocks(proposal.Block, rwSet)
		if err != nil {
			c.log.Panic(err)
		}
	}

	c.preHash = proposal.Block.Header.BlockHash
	c.blockHeight = proposal.Block.Header.BlockHeight
	c.log.Infof("commit height[%d] hash [%s] block success ", c.blockHeight, hex.EncodeToString(c.preHash))
	return
}

// GetLastBlockHeight get last block
func (c *StoreImpl) GetLastBlockHeight() *commonpb.Block {
	var block *commonpb.Block
	var err error
	if !c.isMul {
		block, err = c.store.GetLastBlock()
		if err != nil {
			c.log.Panic(err)
		}
	} else {
		block, err = c.nodeM.GetLastBlockHeights()
		if err != nil {
			c.log.Panic(err)
		}
	}

	return block
}

// GetBlockWithRWSetsByHeight get block
func (c *StoreImpl) GetBlockWithRWSetsByHeight(h uint64) *storePb.BlockWithRWSet {
	var block *storePb.BlockWithRWSet
	var err error
	if !c.isMul {
		block, err = c.store.GetBlockWithRWSets(h)
		if err != nil {
			c.log.Panic(err)
		}
	} else {
		block, err = c.nodeM.GetBlockWithRWSetsByHeights(h)
		if err != nil {
			c.log.Panic(err)
		}
	}

	return block
}

// InitGenesis initialize genesis block
func (c *StoreImpl) InitGenesis(genesisBlock *commonpb.Block, rwSetList []*commonpb.TxRWSet) error {
	gb := &storePb.BlockWithRWSet{Block: genesisBlock, TxRWSets: rwSetList, ContractEvents: nil}
	if !c.isMul {
		if err := c.store.InitGenesis(gb); err != nil {
			return fmt.Errorf("put genesis block failed, %s", err.Error())
		}
	} else {
		if err := c.nodeM.InitGenesiss(gb); err != nil {
			return fmt.Errorf("put genesis block failed, %s", err.Error())
		}
	}

	return nil
}

// SaveCurrentStatus Save CurrentStatus
func (c *StoreImpl) SaveCurrentStatus() {

	err := c.db.UpdateSaveHeightAndNewPre(c.blockHeight, string(c.preHash), c.taskId)

	if err != nil {
		c.log.Error("update status fail ,[error %s]", err)
	}
	atomic.StoreInt32(&c.StartFlag, 0)
}

// SaveCurrentStatus Save CurrentStatus
func (c *StoreImpl) GetCurrentStatus() uint64 {
	return c.blockHeight
}

func RearrangeRWSet(block *commonpb.Block, rwSetMap map[string]*commonpb.TxRWSet) []*commonpb.TxRWSet {
	rwSet := make([]*commonpb.TxRWSet, 0)
	if rwSetMap == nil {
		return rwSet
	}
	for _, tx := range block.Txs {
		if set, ok := rwSetMap[tx.Payload.TxId]; ok {
			rwSet = append(rwSet, set)
		}
	}
	return rwSet
}

func putContractName(db provider.ContractDB, names []string, height uint64, taskId string) error {
	for _, name := range names {

		err := db.InsertContract(name, taskId, height)
		if err != nil {
			return err
		}
	}
	return nil
}

func GetContractList(bstore protocol.BlockchainStore) ([]string, error) {
	contractName := "CONTRACT_MANAGE"

	cmSysContract := make(map[string]bool)
	cmSysContract["ACCOUNT_MANAGER"] = true
	cmSysContract["ARCHIVE_MANAGE"] = true
	cmSysContract["CERT_MANAGE"] = true
	cmSysContract["CHAIN_CONFIG"] = true
	cmSysContract["CHAIN_QUERY"] = true
	cmSysContract["CONTRACT_MANAGE"] = true
	cmSysContract["CROSS_TRANSACTION"] = true
	cmSysContract["DPOS_ERC20"] = true
	cmSysContract["DPOS_STAKE"] = true
	cmSysContract["GOVERNANCE"] = true
	cmSysContract["MULTI_SIGN"] = true
	cmSysContract["PRIVATE_COMPUTE"] = true
	cmSysContract["PUBKEY_MANAGE"] = true
	cmSysContract["SUBSCRIBE_MANAGE"] = true
	cmSysContract["T"] = true

	startKey := []byte(PrefixContractInfo)
	endKey := []byte(PrefixContractInfo)
	endKey[len(endKey)-1] = endKey[len(endKey)-1] + 1

	list := make([]string, 0)
	iter, err := bstore.SelectObject(contractName, startKey, endKey)
	if err != nil {
		return nil, err
	}

	for iter.Next() {
		kv, err := iter.Value()
		if err != nil {
			return nil, err
		}

		keys := strings.Split(string(kv.Key), ":")
		if !cmSysContract[keys[1]] {
			list = append(list, keys[1])
		}
	}

	return list, nil
}
