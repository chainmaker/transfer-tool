/**
 * @Author: starxxliu
 * @Date: 2021/11/19 11:19 上午
 */

package native

import (
	"encoding/json"
	"testing"

	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"github.com/hyperledger/fabric-protos-go/ledger/rwset/kvrwset"
	"github.com/stretchr/testify/require"
)

func TestCreateWriteKeyMap(t *testing.T) {
	txWriteKeyMap := make(map[string]*commonPb.TxWrite, 0) //后期会进行相应的排序
	txWriteKeyMap[constructKey(InstallContractName, []byte("key"))] = &commonPb.TxWrite{
		Key:          []byte("key"),
		Value:        []byte("cdata"),
		ContractName: InstallContractName,
	}
	fWrite := make(map[string][]*kvrwset.KVWrite, 0)
	fWrite["contract"] = make([]*kvrwset.KVWrite, 0)

	fWrite0 := &kvrwset.KVWrite{
		Key:      "key",
		Value:    nil,
		IsDelete: true,
	}
	fWrite["contract"] = append(fWrite["contract"], fWrite0)

	fWrite1 := &kvrwset.KVWrite{
		Key:      "key2",
		Value:    []byte("hello"),
		IsDelete: true,
	}
	fWrite["contract"] = append(fWrite["contract"], fWrite1)

	wByte, err := json.Marshal(fWrite)
	require.Nil(t, err)

	param := make(map[string][]byte, 0)
	param[paramWSetName] = wByte

	err = CreateWriteKeyMap(txWriteKeyMap, param)
	require.Nil(t, err)

	require.Equal(t, len(txWriteKeyMap), 3)
}

func TestCreateDisabledContractList(t *testing.T) {
	txWrite, err := CreateDisabledContractList(nil)
	require.Nil(t, err)
	println(txWrite)
}
