package tbftblock

import (
	"context"
	"encoding/base64"
	"fmt"
	"time"

	"chainmaker.org/chainmaker/transfer-tool/provider"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	consensuspb "chainmaker.org/chainmaker/pb-go/v2/consensus"
	tbftpb "chainmaker.org/chainmaker/pb-go/v2/consensus/tbft"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/transfer-tool/common/msgbus"
	"chainmaker.org/chainmaker/transfer-tool/loggers"
	"chainmaker.org/chainmaker/transfer-tool/utils"
	"github.com/gogo/protobuf/proto"
	"go.uber.org/zap"
)

var (
	DelFaultChanCap      = 100
	TBFTAddtionalDataKey = "TBFTAddtionalDataKey"
)

type TbftBlock struct {
	chainId  string
	Id       string
	hashType string
	log      *zap.SugaredLogger
	signChan chan *provider.TempBlock
	qcChan   chan *provider.TempBlock
	//hashtype crypto.HashType
	//private  crypto.PrivateKey // 证书和公钥身份模式都使用该字段存储私钥
	signer       protocol.SigningMember
	bus          msgbus.MessageBus
	preBlockHash []byte
}

func NewTbftBlock(chainId, Id, hashType string, path string, sign protocol.SigningMember, bus msgbus.MessageBus, signChan chan *provider.TempBlock) *TbftBlock {
	tb := &TbftBlock{}
	tb.log = loggers.GetLogger(loggers.MODULE_CONS)
	tb.chainId = chainId
	tb.Id = Id

	tb.hashType = hashType

	tb.signer = sign
	tb.signChan = signChan
	tb.bus = bus
	tb.qcChan = make(chan *provider.TempBlock, DelFaultChanCap)
	return tb
}

func (tb *TbftBlock) Start(ctx context.Context) {
	go tb.signLoop(ctx)
	go tb.qcLoop(ctx)
}

func (tb *TbftBlock) SetPreHash(hash []byte) {
	tb.preBlockHash = hash
}

func (tb *TbftBlock) GetPreHash() []byte {
	return tb.preBlockHash
}

func (tb *TbftBlock) signLoop(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			tb.log.Infof("close tbft consensus sign loop")
			return
		case spblock := <-tb.signChan:
			startT := time.Now()
			if spblock.Done || spblock.Err != nil {
				tb.qcChan <- spblock
				continue
			}
			spblock.Proposal.Block.Header.PreBlockHash = tb.preBlockHash
			_, err := tb.SignProposedBlock(spblock.Proposal)
			if err != nil { //传到下一层
				tb.log.Errorf("tbft consensus sign proposed block err[%+v]", err)
				spblock.Err = err
				tb.qcChan <- spblock
				continue
			}
			tb.preBlockHash = spblock.Proposal.Block.Header.BlockHash
			tb.qcChan <- spblock
			tb.log.Debugf("tbft consensus sign proposed block[%d] spend time[%+v]", spblock.Height, time.Since(startT))
		}
	}
}

func (tb *TbftBlock) qcLoop(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			tb.log.Infof("close tbft consensus add qc loop")
			return

		case spblock := <-tb.qcChan:
			startT := time.Now()
			if spblock.Done || spblock.Err != nil {
				tb.bus.PublishSafe(msgbus.CommitBlock, spblock)
				continue
			}
			tb.addQc(spblock.Proposal.Block)
			//增加mesBus to store mod
			tb.bus.PublishSafe(msgbus.CommitBlock, spblock)
			tb.log.Debugf("tbft consensus add qc block[%d] spend time[%+v]", spblock.Height, time.Since(startT))
		}
	}
}

func (tb *TbftBlock) SendMes(err error, done bool) {
	proposal := &provider.TempBlock{0, nil, nil, nil, nil, done, err, ""}
	tb.bus.PublishSafe(msgbus.CommitBlock, proposal)
}

func (tb *TbftBlock) HandleProposedBlock(proposedBlock *consensuspb.ProposalBlock) (*common.Block, error) {
	startT := time.Now()
	signedBlock, err := tb.SignProposedBlock(proposedBlock)
	if err != nil {
		return nil, err
	}
	startQ := time.Now()
	finalBlock := tb.addQc(signedBlock)
	tb.log.Debugf("HandleProposedBlock spend time Sign Proposed Block [%d]us add qc[%+v]", startQ.Sub(startT).Microseconds(), time.Since(startQ))
	return finalBlock, nil
}

func (tb *TbftBlock) SignProposedBlock(proposedBlock *consensuspb.ProposalBlock) (*common.Block, error) {

	block := proposedBlock.Block

	blockHash, sig, err := utils.SignBlock(tb.hashType, tb.signer, block)
	if err != nil {
		return nil, fmt.Errorf("CalcBlockHash failed error:%v", err)
	}

	block.Header.BlockHash = blockHash[:]
	block.Header.Signature = sig

	return block, nil

}

// mustMarshal marshals protobuf message to byte slice or panic
func mustMarshal(msg proto.Message) []byte {
	data, err := proto.Marshal(msg)
	if err != nil {
		panic(err)
	}
	return data
}

func (tb *TbftBlock) addQc(block *common.Block) *common.Block {

	//1。生成voteProto
	voteProto := &tbftpb.Vote{
		Type:   tbftpb.VoteType_VOTE_PRECOMMIT,
		Voter:  tb.Id,
		Height: block.Header.BlockHeight,
		Round:  0,
		Hash:   block.Header.BlockHash,
	}

	//2.序列化voteProto
	voteBytes := mustMarshal(voteProto)

	//3.对voteProto进行签名
	sig, err := tb.signer.Sign(tb.hashType, voteBytes)
	if err != nil {
		panic(err)
	}

	serializeMember, err := tb.signer.GetMember()
	if err != nil {
		panic(err)
	}

	voteProto.Endorsement = &common.EndorsementEntry{
		Signer:    serializeMember,
		Signature: sig,
	}

	//4、生成VsProto
	vsProto := &tbftpb.VoteSet{
		Type:         tbftpb.VoteType_VOTE_PRECOMMIT,
		Height:       block.Header.BlockHeight,
		Round:        0,
		Sum:          1,
		Maj23:        block.Header.BlockHash,
		Votes:        make(map[string]*tbftpb.Vote),
		VotesByBlock: make(map[string]*tbftpb.BlockVotes),
	}
	//5、添加VoteProto到vsProto
	vsProto.Votes[voteProto.Voter] = voteProto

	//6、添加VoteByBlock到vsProto
	hashStr := base64.StdEncoding.EncodeToString(voteProto.Hash)

	//6.1、生成BlockVtoesProto
	bvProto := &tbftpb.BlockVotes{
		Votes: make(map[string]*tbftpb.Vote),
	}
	bvProto.Votes[voteProto.Voter] = voteProto
	bvProto.Sum = 1

	vsProto.VotesByBlock[hashStr] = bvProto

	qc := mustMarshal(vsProto)

	if block.AdditionalData == nil {
		block.AdditionalData = &common.AdditionalData{
			ExtraData: make(map[string][]byte),
		}
	}

	block.AdditionalData.ExtraData[TBFTAddtionalDataKey] = qc
	return block
}
