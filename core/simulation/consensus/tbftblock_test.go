package tbftblock

import (
	"fmt"
	"testing"
	"time"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	consensuspb "chainmaker.org/chainmaker/pb-go/v2/consensus"
)

var (
	chainId = "chain1"
)

func TestCreatTbftBlock(t *testing.T) {

	b1 := CreateBlock(chainId, 1)

	pb1 := &consensuspb.ProposalBlock{
		Block: b1,
	}

	tb := NewTbftBlock("", "", "", "", nil, nil, nil)

	signedBlock, err := tb.SignProposedBlock(pb1)
	if err != nil {
		t.Errorf("SignProposedBlock %v", err)
	}

	finalBlock := tb.addQc(signedBlock)

	t.Logf("generate final TBFT block %v", finalBlock.String())
}

func CreateBlock(chainid string, height uint64) *common.Block {

	block := &common.Block{
		Header: &common.BlockHeader{
			ChainId:     chainid,
			BlockHeight: height,
			Signature:   []byte(""),
			BlockHash:   []byte(""),
		},
		Dag: &common.DAG{},
		Txs: []*common.Transaction{
			{
				Payload: &common.Payload{
					ChainId: chainid,
				},
			},
		},
	}

	// blockHash := sha256.Sum256([]byte(fmt.Sprintf("%s-%d", chainid, height)))
	blockHash := []byte(fmt.Sprintf("%s-%d-%s", chainid, height, time.Now()))
	block.Header.BlockHash = blockHash[:]

	// txHash := sha256.Sum256([]byte(fmt.Sprintf("%s-%d", blockHash, 0)))
	// block.Txs[0].Payload.TxId = string(txHash[:])

	return block
}
