package core

import (
	"context"
	"encoding/hex"
	"fmt"
	"strings"
	"sync/atomic"

	"chainmaker.org/chainmaker/transfer-tool/chainMaker"
	"chainmaker.org/chainmaker/transfer-tool/common"
	"chainmaker.org/chainmaker/transfer-tool/common/msgbus"
	"chainmaker.org/chainmaker/transfer-tool/config"
	"chainmaker.org/chainmaker/transfer-tool/core/creator"
	"chainmaker.org/chainmaker/transfer-tool/core/simulation"
	"chainmaker.org/chainmaker/transfer-tool/core/store"
	"chainmaker.org/chainmaker/transfer-tool/fabric"
	"chainmaker.org/chainmaker/transfer-tool/loggers"
	"chainmaker.org/chainmaker/transfer-tool/provider"
	"go.uber.org/zap"
)

var (
	defaultBlocks = 548
)

type TransferImpl struct {
	chainId   string
	taskId    string
	logger    *zap.SugaredLogger
	ResChan   chan provider.TransferRes
	config    *config.Config
	cancel    context.CancelFunc
	ctx       context.Context
	store     *store.StoreImpl
	core      *simulation.CoreEngine
	txCreator *creator.TxCreator
	db        provider.TransferDB
}

// NewTransfer new transferImpl
func NewTransfer(conf *config.Config, db provider.TransferDB, res chan provider.TransferRes, errC chan error,
	taskId string) (*TransferImpl, error) { //可以是一个支持查询与写入的接口

	config.TransferConfig = conf
	transfer := &TransferImpl{
		chainId: conf.Chain.ChainId,
		taskId:  taskId,
		config:  conf,
		ResChan: res,
		db:      db,
	}
	//New message bus
	msg := msgbus.NewMessageBus()
	msgC := msgbus.NewMessageBus()

	ctx, cancel := context.WithCancel(context.Background())
	transfer.cancel = cancel
	transfer.ctx = ctx
	loggers.SetLogConfig(&conf.LogConfig)
	//storeImpl init
	contractList, err := db.GetContracts(taskId)
	if err != nil {
		return nil, err
	}

	checker, err := getChecker(conf.OriginChain.ChainType, conf.OriginChain.ChainVersion, contractList)
	if err != nil {
		return nil, err
	}

	plog := loggers.NewProtocolLog(loggers.GetLogger(loggers.MODULE_SAVE))
	store, err := store.NewStoreImpl2(msgC, plog, errC, ctx, checker, db, taskId)
	if err != nil {
		return nil, err
	}
	transfer.store = store

	err = transfer.initStatus(store, db, taskId)
	if err != nil {
		return nil, err
	}

	saveHeight, err := db.GetSaveHeight(taskId)
	if err != nil {
		return nil, err
	}

	newPre, err := db.GetNewPreHash(taskId)
	if err != nil {
		return nil, err
	}

	certHash, err := db.GetCertHash(taskId)
	if err != nil {
		return nil, err
	}

	sim, err := getSimContract(conf.OriginChain.ChainType, conf.OriginChain.ChainVersion)
	if err != nil {
		return nil, err
	}
	core, err := simulation.NewCoreEngine(saveHeight, []byte(newPre), errC, msg, msgC, sim) //通过这里传递相应的createWriteKeyMap and CreateTxWriteKeyMap
	if err != nil {
		return nil, err
	}
	transfer.core = core

	tBatch, err := getTxBatch(conf.OriginChain.ChainType, conf.OriginChain.ChainVersion, certHash, conf, db)
	if err != nil {
		return nil, err
	}

	send := make(chan *provider.SendInfo, defaultBlocks) //这里需要修改，可以从外部get到
	fetcher, err := getBlockFetcher(conf.OriginChain.ChainType, conf.OriginChain.ChainVersion, newPre, saveHeight, send, conf)
	if err != nil {
		return nil, err
	}

	txCreator, err := creator.NewParserImpl2(msg, db, core.Proposer, tBatch, fetcher, send, taskId)
	if err != nil {
		return nil, err
	}
	transfer.txCreator = txCreator

	return transfer, nil
}

func (t *TransferImpl) Start() error {
	err := t.writeFirstBlock()
	if err != nil {
		return err
	}

	t.store.Start()
	t.core.Start(t.ctx)
	err = t.txCreator.Start(t.ctx)

	if err != nil {
		t.store.Close(nil)
		t.cancel()
		return err
	}
	return nil
}

func (t *TransferImpl) Stop() error {
	t.store.SaveCurrentStatus()

	if atomic.LoadInt32(&t.store.StartFlag) == int32(1) {
		t.store.Close(nil)
	}

	t.cancel()
	return nil
}

func (t *TransferImpl) IsFinish() bool {
	return false
}

func (t *TransferImpl) GetStatus() uint64 {
	return t.store.GetCurrentStatus()
}

func (t *TransferImpl) GetCurrentTransferHeight() (last, current uint64) {
	last, _ = t.txCreator.GetFetcher().GetLastBlockHeight()
	return last, t.store.GetCurrentStatus()
}

func (t *TransferImpl) GetStore() *store.StoreImpl {
	return t.store
}

func (t *TransferImpl) GetCore() *simulation.CoreEngine {
	return t.core
}

func (t *TransferImpl) GetTxCreator() *creator.TxCreator {
	return t.txCreator
}

func GetOriginChainFetch(chainType, version, name, confPath, userName string) (provider.Fetcher, error) {

	if chainType == "fabric" {

		config := &config.Config{
			OriginChain: config.OriginChain{
				Fabric: config.FabricInfo{
					ChainName:  name,
					UserName:   userName,
					ConfigPath: confPath,
				},
			},
		}
		return getBlockFetcher(chainType, version, "", 0, nil, config)

	} else if chainType == "chainmaker" {

		config := &config.Config{
			OriginChain: config.OriginChain{
				ChainMaker: config.ChainMakerInfo{
					ChainName:  name,
					UserName:   userName,
					ConfigPath: confPath,
				},
			},
		}
		return getBlockFetcher(chainType, version, "", 0, nil, config)

	} else {

		return nil, fmt.Errorf("unsupport chainType")
	}
}

func (t *TransferImpl) writeFirstBlock() error {
	block := t.store.GetLastBlockHeight()

	if block.Header.BlockHeight == 0 {
		tb, err := t.txCreator.PreFirstBlock()
		if err != nil {
			return err
		}
		proposal, err := t.core.PreFirstBlock(tb)
		if err != nil {
			return err
		}

		t.store.PutBlock(proposal)

		//if crtHash is nil,update db
		crtHash := t.txCreator.GetTxBatch().GetChainMakerClientUserCrtHash()
		if len(crtHash) > 0 {
			err = t.db.UpdateUserCrtHash(hex.EncodeToString(crtHash), t.taskId)
			if err != nil {
				return err
			}
			common.ShortCerts[hex.EncodeToString(crtHash)] =
				t.txCreator.GetTxBatch().GetUserCrtByte()
		}
	} else {
		t.store.SetStatus(block)
	}

	return nil
}

func (t *TransferImpl) initStatus(store *store.StoreImpl, db provider.TransferDB, taskId string) error {
	//store.GetBlockWithRWSetsByHeight(1)
	block := store.GetLastBlockHeight()

	_, err := db.GetSaveHeight(taskId)
	if err != nil {
		if err.Error() == "record not found" && block == nil {

			status, err := t.writeGenesisBlock(store)
			if err != nil {
				return err
			}

			err = db.InsertStatus(status.SaveHeight, status.NewPreHash, t.taskId)
			if err != nil {
				return err
			}

		} else if err.Error() == "record not found" && block != nil {
			store.Close(nil)
			return fmt.Errorf("sql database is nil,but chainmaker data not nil,err %v", err)
		}
	} else if block == nil {
		status, err := t.writeGenesisBlock(store)
		if err != nil {
			return err
		}
		db.UpdateSaveHeightAndNewPre(status.SaveHeight, status.NewPreHash, t.taskId)
		db.UpdateUserCrtHash(status.CertHash, t.taskId)
		_, err = db.GetCertHash(t.taskId)
		if err != nil {
			return err
		}

	}

	err = nil
	//目标链的存储模块生成了创世块后
	block = store.GetLastBlockHeight()

	//最终是以写入的block 为准
	saveHight, err := db.GetSaveHeight(t.taskId)
	if err != nil {
		return err
	}
	if saveHight != block.Header.BlockHeight {

		err := db.UpdateSaveHeightAndNewPre(block.Header.BlockHeight, string(block.Header.BlockHash), t.taskId)
		if err != nil {
			return err
		}
	}

	//init store model
	//initChainConfig(store)

	return nil

}

type Status struct {
	SaveHeight      uint64 `gorm:"column:save_height"`
	PreHash         string `gorm:"column:pre_hash"`
	NewPreHash      string `gorm:"column:new_pre_hash"` //chainMaker pre hash
	EnableShortCert bool   `gorm:"column:enable_short_cert"`
	CertHash        string `gorm:"column:cert_hash"`
}

func (t *TransferImpl) writeGenesisBlock(store *store.StoreImpl) (*Status, error) {
	block, TxRWSet, err := common.GetGenesisBlock(t.config.Chain.Genesis)
	if err != nil {
		return nil, err
	}
	err = store.InitGenesis(block, TxRWSet)
	if err != nil {
		return nil, err
	}

	return &Status{
		SaveHeight: block.Header.BlockHeight,
		PreHash:    "",
		NewPreHash: string(block.Header.BlockHash),
	}, nil

}

// create chainMaker TxBatch creator
func getTxBatch(chainType, version, certHash string, config *config.Config, db provider.TransferDB) (provider.ChainMakerTxBatch, error) {
	chainType = strings.ToLower(chainType)
	version = strings.ToLower(version)

	logger := loggers.GetLogger(loggers.MODULE_PARSER)

	var certHashB []byte
	var err error
	if len(certHash) > 0 {
		certHashB, err = hex.DecodeString(certHash)
		if err != nil {
			return nil, err
		}
	}
	switch chainType {
	case "fabric":
		return fabric.NewTxsCreator(config, logger, version, certHashB, db)
	case "chainmaker":
		return chainMaker.NewTxsCreator(config, logger, version, certHashB, db)
	default:
		return nil, fmt.Errorf("nonsupport chain ")
	}
}

//get chainMaker block creator
func getBlockFetcher(chainType, version, preHash string, height uint64, send chan *provider.SendInfo, config *config.Config) (provider.Fetcher, error) {
	chainType = strings.ToLower(chainType)
	version = strings.ToLower(version)

	logger := loggers.GetLogger(loggers.MODULE_PARSER)

	switch chainType {
	case "fabric":
		return fabric.NewFetcher(version, provider.WithFetcherConfigLogger(logger),
			provider.WithFetcherConfigChainId(config.OriginChain.Fabric.ChainName),
			provider.WithFetcherConfigHeight(height),
			provider.WithFetcherConfigPre([]byte(preHash)),
			provider.WithFetcherConfigSendChan(send),
			provider.WithFetcherConfigPath(config.OriginChain.Fabric.ConfigPath),
			provider.WithFetcherConfigUserName(config.OriginChain.Fabric.UserName)) //这里使用opt的模式

	case "chainmaker":
		return chainMaker.NewFetcher(version, provider.WithFetcherConfigLogger(logger),
			provider.WithFetcherConfigChainId(config.OriginChain.ChainMaker.ChainName),
			provider.WithFetcherConfigHeight(height),
			provider.WithFetcherConfigPre([]byte(preHash)),
			provider.WithFetcherConfigSendChan(send),
			provider.WithFetcherConfigPath(config.OriginChain.ChainMaker.ConfigPath))
	default:
		return nil, fmt.Errorf("nonsupport chain ")
	}
}

//get simulation contract instance
func getSimContract(chainType, version string) (provider.SimContractExec, error) {
	chainType = strings.ToLower(chainType)
	version = strings.ToLower(version)

	switch chainType {
	case "fabric":
		return fabric.NewSimContract(version)
	case "chainmaker":
		return chainMaker.NewSimContract(version)
	default:
		return nil, fmt.Errorf("nonsupport chain ")
	}
}

func getChecker(chainType, version string, contractList map[string]uint64) (provider.Checker, error) {
	chainType = strings.ToLower(chainType)
	version = strings.ToLower(version)

	switch chainType {
	case "fabric":
		return fabric.NewChecker(version, contractList)
	case "chainmaker":
		return chainMaker.NewChecker(version, contractList)
	default:
		return nil, fmt.Errorf("nonsupport chain ")
	}
}
