/**
 * @Author: starxxliu
 * @Date: 2021/12/9 11:05 上午
 */

package chainMaker

import (
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCopyAndCompressFile(t *testing.T) {
	_, err := CreateContract("../testdata", "transfer", "mycc")
	require.Nil(t, err)
}

func TestSplitAfter(t *testing.T) {
	paths := strings.SplitAfterN("/Users/gopath/src/github.com", string(filepath.Separator), 2)
	println(paths)
}
