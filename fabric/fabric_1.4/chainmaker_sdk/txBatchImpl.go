/**
 * @Author: starxxliu
 * @Date: 2021/11/16 10:18 上午
 */

package chainmaker_sdk

import (
	commonl "chainmaker.org/chainmaker/transfer-tool/common"
	"chainmaker.org/chainmaker/transfer-tool/provider"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"time"

	chainMaker "chainmaker.org/chainmaker/transfer-tool/common/chainmaker-sdk"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/transfer-tool/config"
	"chainmaker.org/chainmaker/transfer-tool/db/sqlite"
	"chainmaker.org/chainmaker/transfer-tool/fabric/fabric_1.4/fabric"
	"chainmaker.org/chainmaker/transfer-tool/utils"
	commonf "github.com/hyperledger/fabric-protos-go/common"
	"github.com/hyperledger/fabric-protos-go/peer"
	"go.uber.org/zap"
)

const (
	lscc        = "lscc"
	lsccInstall = "install"
	lsccDeploy  = "deploy"
	lsccUpgrade = "upgrade"
	escc        = "escc"
	vscc        = "vscc"
	SysLscc     = "lscc"
)

var (
	startHeight     = 0 //记录我们迁移的起始高度
	defaultVersion  = "v1.0.0"
	runtime         = common.RuntimeType_DOCKER_GO
	limit           = 10
	invoke_contract = "invoke_contract"
)

type TxBatchImpl struct {
	contractName       map[string]uint64 //key is contractName value is install block height
	client             *chainMaker.ChainClient
	log                *zap.SugaredLogger
	assistContractName string
	transferMethod     string
	userContractPath   string
	assistContractPath string
	authType           string
	db                 provider.TransferDB
}

func NewTxBatchImpl(config *config.Config, log *zap.SugaredLogger, certHash []byte, db provider.TransferDB) (*TxBatchImpl, error) {
	permissioned := chainMaker.StringToAuthTypeMap[config.Chain.AuthType]

	enableShortCert := false
	if len(certHash) > 0 {
		enableShortCert = true
	}
	client, err := chainMaker.NewChainClient(
		chainMaker.WithChainClientOrgId(config.Chain.OrgId),
		chainMaker.WithChainClientChainId(config.Chain.ChainId),
		chainMaker.WithChainClientLogger(log),
		chainMaker.WithUserSignKeyFilePath(config.Chain.UserSignKeyFilePath),
		chainMaker.WithUserSingCrtFilePath(config.Chain.UserSignCrtFilePath),

		chainMaker.WithHashType(config.Chain.HashType),
		chainMaker.WithAuthType(permissioned),
		//支持短证书交易
		chainMaker.WithEnabledCrtHash(enableShortCert),
		chainMaker.WithUserCrtHash(certHash),
	)
	if err != nil {
		return nil, err
	}

	if enableShortCert && len(client.GetClientUserCrtHash()) > 0 {
		commonl.ShortCerts[hex.EncodeToString(certHash)] = client.GetClientUserCrtBytes()
	}

	tx := &TxBatchImpl{
		//contractName:       conn,
		client:             client,
		log:                log,
		assistContractPath: config.ContractInfo.AssistContractPath,
		userContractPath:   config.ContractInfo.UserContractPath,
		assistContractName: config.ContractInfo.AssistContractName,
		transferMethod:     config.ContractInfo.TransferMethod,
		authType:           config.Chain.AuthType,
		db:                 db,
	}
	return tx, nil
}

//CreateTxBatch create chainmaker tx for fabric block
func (tb *TxBatchImpl) CreateTxBatch(bInfo *fabric.BlockInfo) ([]*common.Transaction, error) {
	txBatch := make([]*common.Transaction, 0, len(bInfo.Envs))
	tb.log.Debugf("start create tx batch fabric height[%d] txs[%d] ", bInfo.Block.Header.Number, len(bInfo.Envs))

	if len(bInfo.Envs) != len(bInfo.TxValidationCode) {
		return nil, errors.New("tx numbers not match tx validation code")
	}

	if bInfo.IsConfig {
		isValid := false
		if bInfo.TxValidationCode[0] == peer.TxValidationCode_VALID {
			isValid = true
		}
		req, err := tb.createTransferAssistTx(bInfo.Envs[0], bInfo.Block.Data.Data[0], true, isValid,
			&common.Limit{GasLimit: 0}, bInfo.TxValidationCode[0])
		if err != nil {
			return nil, err
		}

		txBatch = append(txBatch, req)
		return txBatch, nil
	}

	for k, v := range bInfo.Envs {
		if bInfo.TxValidationCode[k] != peer.TxValidationCode_VALID {
			req, err := tb.createTransferAssistTx(v, bInfo.Block.Data.Data[k], false, false,
				&common.Limit{GasLimit: 0}, bInfo.TxValidationCode[k])
			if err != nil {
				return nil, err
			}
			txBatch = append(txBatch, req)
			continue
		}

		cName := v.Payload.Transaction.ChaincodeAction.Proposal.Input.ChaincodeId.Name
		//if !tb.filterContractName(cName) {  //因为我们这里并行处理，所以将这里的验证，放在最后
		//	return nil, fmt.Errorf("fabric height[%d] a call to an unknown contract occurred [%s] ",bInfo.Block.Header.Number,cName)
		//}
		if cName == SysLscc {
			req, err := tb.createLsccTx(v, bInfo.Block.Data.Data[k], bInfo.Block.Header.Number, bInfo.TxValidationCode[k])
			if err != nil {
				return nil, err
			}

			txBatch = append(txBatch, req)
			continue
		}
		// 普通的交易调用
		req, err := tb.createNormalTx(v, bInfo.Block.Data.Data[k], false, true,
			false, &common.Limit{GasLimit: 0}, bInfo.TxValidationCode[k])
		if err != nil {
			return nil, err
		}
		txBatch = append(txBatch, req)
	}
	tb.log.Debugf("end create tx batch fabric height[%d] txs[%d] ", bInfo.Block.Header.Number, len(bInfo.Envs))
	return txBatch, nil
}

/*
	进行first block的构建我们需要将genesis block与fabric高度为1的block合并构建到一个区块中。其中我们只需要将fabric genesis tx,作为
	安装辅助合约交易的一个参数。
	fabric block 1 存在两种情况
	1: fabric block 1 is config block. 我们将config tx同样设为安装辅助合约交易的一个参数。
	2: fabric block 1 is lscc tx.(其中只会是deploy合约交易). 转为正常的chainmaker安装合约交易。
*/
func (tb *TxBatchImpl) CreateFirstTxBatch(bs []*commonf.Block) ([]*common.Transaction, error) {
	txBatch := make([]*common.Transaction, 0, 2)
	tb.log.Debugf("start create first chainmaker txBatch ")
	//1. 生成添加短证书交易，使用public模式暂时不使用短证书
	if tb.authType == "permissionedwithcert" { //修改为小写
		tx1, err := tb.createShutCertTx()
		if err != nil {
			return nil, err
		}
		txBatch = append(txBatch, tx1)
	}

	kvs := make([]*common.KeyValuePair, 0)
	kv := &common.KeyValuePair{
		Key:   "genesis_tx",
		Value: bs[0].Data.Data[0],
	}
	kvs = append(kvs, kv)

	txs, origins, err := tb.handleFirstBlock(bs[1])
	if err != nil {
		return nil, err
	}
	for k, v := range origins {
		key := "origin_tx_" + strconv.Itoa(k+1)
		kv := &common.KeyValuePair{
			Key:   key,
			Value: v,
		}
		kvs = append(kvs, kv)
	}

	req2, err := tb.createTransferContractTx(kvs, &common.Limit{GasLimit: 0})
	if err != nil {
		return nil, err
	}
	txBatch = append(txBatch, req2)

	txBatch = append(txBatch, txs...)
	//update
	err = tb.updateClient()
	if err != nil {
		return nil, err
	}

	tb.log.Debugf("end create first chainmaker txBatch ")
	return txBatch, nil
}

func (tb *TxBatchImpl) handleFirstBlock(block *commonf.Block) ([]*common.Transaction, [][]byte, error) {
	b, err := fabric.ParseBlock(block)
	if err != nil {
		return nil, nil, err
	}

	txBatch := make([]*common.Transaction, 0, 0)
	origins := make([][]byte, 0, 0)
	if b.IsConfig {
		origins = append(origins, block.Data.Data[0])
		return nil, origins, nil
	}

	for k, v := range b.Envs {
		if b.TxValidationCode[k] != peer.TxValidationCode_VALID {
			origins = append(origins, block.Data.Data[k])
			continue
		}
		// 有效交易，我们过滤合约Name，防止出现对未知合约的调用
		cName := v.Payload.Transaction.ChaincodeAction.Proposal.Input.ChaincodeId.Name
		//if !tb.filterContractName(cName) {
		//	return nil, nil, errors.New("a call to an unknown contract occurred")
		//}
		if cName == SysLscc {
			req, err := tb.createLsccTx(v, b.Block.Data.Data[k], b.Block.Header.Number, b.TxValidationCode[k])
			if err != nil {
				return nil, nil, err
			}

			args := v.Payload.Transaction.ChaincodeAction.Proposal.Input.Input.Args
			if string(args[0]) == lsccDeploy {
				spec, err := fabric.ParseChaincodeSpec(args[2])
				if err != nil {
					return nil, nil, err
				}
				err = tb.insertContract(spec.ChaincodeId.Name, block.Header.Number)
				if err != nil {
					return nil, nil, err
				}
			}

			txBatch = append(txBatch, req)
			continue
		}
		// 普通的交易调用
		//req, err := tb.createNormalTx(v, b.Block.Data.Data[k], false, true,
		//	false,&common.Limit{GasLimit: 0},b.TxValidationCode[k])
		//if err != nil {
		//	return nil, nil, err
		//}
		//txBatch = append(txBatch, req)
	}
	return txBatch, origins, nil
}

func (tb *TxBatchImpl) GetUserCrtByte() []byte {
	return tb.client.GetClientUserCrtBytes()
}

func (tb *TxBatchImpl) GetClientUserCrtHash() []byte {
	return tb.client.GetClientUserCrtHash()
}

//updateClient 2.2.0 version use pk
func (tb *TxBatchImpl) updateClient() error {
	if tb.client.GetClientAuth() == chainMaker.Public {
		return nil
	}
	err := tb.client.UpdateCertHash(true, nil)
	if err != nil {
		return err
	}

	//tb.db.UpdateUserCrtHash(string(tb.client.GetClientUserCrtBytes()),tb.)
	//db := sqlite.GetDB()
	//status, err := sqlite.GetStatus(db, "1")
	//if err != nil {
	//	return err
	//}
	//
	//status.EnableShortCert = true
	//status.CertHash = string(tb.client.GetClientUserCrtBytes())

	//err = status.UpdateChain(db)
	//if err != nil {
	//	return err
	//}
	return nil
}

func (tb *TxBatchImpl) createLsccTx(env *fabric.Envelope, originTx []byte, height uint64, code peer.TxValidationCode) (*common.Transaction, error) {
	args := env.Payload.Transaction.ChaincodeAction.Proposal.Input.Input.Args
	if string(args[0]) == lsccInstall { //install 交易不会入块
		return nil, fmt.Errorf("enter error code")
	} else if string(args[0]) == lsccDeploy {
		//在fabric中install交易不会入块,所以fabric deploy对应chainmaker deploy
		spec, err := fabric.ParseChaincodeSpec(args[2])
		if err != nil {
			return nil, err
		}
		tb.log.Debugf("create deploy[%s] contract tx  ", spec.ChaincodeId.Name)

		tx, err := tb.createUserContractTx(env, originTx, false, spec.ChaincodeId.Name, true,
			true, &common.Limit{GasLimit: 0}, code)
		if err != nil {
			return nil, err
		}
		//tb.contractName[spec.ChaincodeId.Name] = height
		return tx, nil

	} else if string(args[0]) == lsccUpgrade {
		tb.log.Debugf("upgrade contract tx to normal contract invoke tx ")

		return tb.createNormalTx(env, originTx, false, true, false, &common.Limit{GasLimit: 0}, code)
	} else {
		return nil, errors.New("unknown lscc contract functionName")
	}
	return nil, nil
}

func (tb *TxBatchImpl) createTransferAssistTx(env *fabric.Envelope, originTx []byte, isConfig, isValid bool,
	limit *common.Limit, code peer.TxValidationCode) (*common.Transaction, error) {
	kvs, err := tb.createKvs(env, originTx, []byte(tb.transferMethod), isConfig, isValid, false, code)
	if err != nil {
		return nil, err
	}
	txId := ""
	if code != peer.TxValidationCode_DUPLICATE_TXID {
		txId = env.Payload.Header.ChannelHeader.TxId
	}
	req, err := tb.client.InvokeContract(tb.assistContractName, invoke_contract, txId,
		kvs, 0, false, limit, time.Now().Unix())
	if err != nil {
		return nil, err
	}

	tx := &common.Transaction{
		Payload:   req.Payload,
		Sender:    req.Sender,
		Endorsers: req.Endorsers,
		Result:    nil}
	return tx, nil
}

func (tb *TxBatchImpl) createShutCertTx() (*common.Transaction, error) {
	req, err := tb.client.AddCert()
	if err != nil {
		return nil, err
	}
	tx := &common.Transaction{
		Payload:   req.Payload,
		Sender:    req.Sender,
		Endorsers: req.Endorsers,
		Result:    nil}
	return tx, nil
}

func (tb *TxBatchImpl) createTransferContractTx(kvs []*common.KeyValuePair, limit *common.Limit) (*common.Transaction, error) {
	req, err := tb.client.CreateContractCreate(tb.assistContractName, "", defaultVersion, tb.assistContractPath,
		runtime, kvs, limit, time.Now().Unix())
	if err != nil {
		return nil, err
	}

	tx := &common.Transaction{
		Payload:   req.Payload,
		Sender:    req.Sender,
		Endorsers: req.Endorsers,
		Result:    nil}
	return tx, nil
}

func (tb *TxBatchImpl) createUserContractTx(env *fabric.Envelope, originTx []byte, isConfig bool, contractName string,
	isValid, isContract bool, limit *common.Limit, code peer.TxValidationCode) (*common.Transaction, error) {

	kvs, err := tb.createKvs(env, originTx, nil, isConfig, isValid, isContract, code)
	if err != nil {
		return nil, err
	}
	txId := env.Payload.Header.ChannelHeader.TxId
	req, err := tb.client.CreateContractCreate(contractName, txId, defaultVersion, tb.userContractPath,
		runtime, kvs, limit, time.Now().Unix())
	if err != nil {
		return nil, err
	}
	tx := &common.Transaction{
		Payload:   req.Payload,
		Sender:    req.Sender,
		Endorsers: req.Endorsers,
		Result:    nil}
	return tx, nil
}

func (tb *TxBatchImpl) createNormalTx(env *fabric.Envelope, originTx []byte, isConfig, isValid, isContract bool,
	limit *common.Limit, code peer.TxValidationCode) (*common.Transaction, error) {

	kvs, err := tb.createKvs(env, originTx, []byte(tb.transferMethod), isConfig, isValid, isContract, code)
	if err != nil {
		return nil, err
	}

	req, err := tb.client.InvokeContract(tb.assistContractName, invoke_contract, env.Payload.Header.ChannelHeader.TxId,
		kvs, 0, false, limit, time.Now().Unix())
	if err != nil {
		return nil, err
	}

	tx := &common.Transaction{
		Payload:   req.Payload,
		Sender:    req.Sender,
		Endorsers: req.Endorsers,
		Result:    nil}
	return tx, nil
}

func (tb *TxBatchImpl) filterContractName(name string) bool {
	if name == SysLscc {
		return true
	}
	return false
}

func (tb *TxBatchImpl) filterSysContractName(name string) bool {
	if name == SysLscc {
		return true
	}
	return false
}

func (tb *TxBatchImpl) createKvs(env *fabric.Envelope, originTx, method []byte, isConfig, isValid,
	isContract bool, code peer.TxValidationCode) ([]*common.KeyValuePair, error) {
	is_valid := "valid"
	if !isValid {
		is_valid = "invalid"
	}
	is_config := "normal"
	if isConfig {
		is_config = "config"
	}
	kvs := []*common.KeyValuePair{
		{
			Key:   "origin_tx",
			Value: originTx,
		},
		{
			Key:   "is_valid",
			Value: []byte(is_valid),
		},
		{
			Key:   "tx_type",
			Value: []byte(is_config),
		},
	}

	if code == peer.TxValidationCode_DUPLICATE_TXID {
		kvs = append(kvs, &common.KeyValuePair{
			Key:   "origin_txid",
			Value: []byte(env.Payload.Header.ChannelHeader.TxId),
		},
		)
	}
	if method != nil {
		kvs = append(kvs, &common.KeyValuePair{
			Key:   "method",
			Value: method,
		},
		)
	}
	var tx_wset []byte
	var err error
	if isContract { //表示当前交易属于实例化合约或者是升级合约交易，提取读写集
		if !isValid || isConfig {
			tx_wset = nil
		} else {
			wSet := make(map[string][]byte, 0)
			for _, v := range env.Payload.Transaction.ChaincodeAction.Response.TxRwSet.NsRwSets {
				if tb.filterSysContractName(v.NameSpace) {
					continue
				}
				for _, wSet := range v.KvRwSet.Writes {
					wSet.Key, err = utils.FixKey(wSet.Key)
					if err != nil {
						return nil, err
					}
					err = checkKey(wSet.Key)
					if err != nil {
						return nil, err
					}
				}

				wsetByte, err := json.Marshal(v.KvRwSet.Writes)
				if err != nil {
					return nil, err
				}

				wSet[v.NameSpace] = wsetByte
			}

			tx_wset, err = json.Marshal(wSet)
			if err != nil {
				return nil, err
			}
			kvNC := []*common.KeyValuePair{
				{
					Key:   "tx_wset",
					Value: tx_wset,
				},
			}

			kvs = append(kvs, kvNC...)
		}
	}
	return kvs, nil
}

func checkParameters(reqBatch []*common.Transaction) error {
	for _, req := range reqBatch {
		for _, p := range req.Payload.Parameters {
			match, err := regexp.MatchString(protocol.DefaultStateRegex, p.Key)
			if err != nil || !match {
				return fmt.Errorf(
					"expect key no special characters, but got key:[%s]. letter, number, dot and underline are allowed",
					p.Key)
			}
		}
	}
	return nil
}

func checkKey(key string) error {
	match, err := regexp.MatchString(protocol.DefaultStateRegex, key)
	if err != nil || !match {
		return fmt.Errorf(
			"expect key no special characters, but got key:[%s]. letter, number, dot and underline are allowed",
			key)
	}
	return nil
}

func (tb *TxBatchImpl) insertContract(name string, height uint64) error {
	db := sqlite.GetDB()
	contract := sqlite.Contract{
		ContractName: name,
		Height:       height,
	}
	_, err := contract.InsertContract(db)
	if err != nil {
		return err
	}

	return nil
}
