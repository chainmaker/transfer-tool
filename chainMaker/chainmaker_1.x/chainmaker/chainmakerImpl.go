/**
 * @Author: starxxliu
 * @Date: 2021/11/16 10:00 上午
 */

package chainmaker

import (
	"context"
	"errors"
	"fmt"
	"sync/atomic"
	"time"

	"chainmaker.org/chainmaker-go/pb/protogo/common"
	"chainmaker.org/chainmaker/transfer-tool/common/transfer"
	configL "chainmaker.org/chainmaker/transfer-tool/config"
	"chainmaker.org/chainmaker/transfer-tool/provider"
	"github.com/panjf2000/ants/v2"
	"go.uber.org/zap"
)

var (
	retryNum       = 10
	RoundNum       = 1000
	ThresholdRound = 500 //轮次阀值，当消费者每一个轮次消费对应阀值的block后 fetcher 进入下一轮次
)

//Fetcher fabric block Fetcher
type Fetcher struct {
	Fetch             *fetchChainmakerBySdk
	SendBlockInfo     chan *provider.SendInfo
	LatestBlockHeight uint64
	QueryHeight       uint64 //当前的迁移高度
	CurrentHeight     uint64 //当前的迁移高度
	StartHeight       uint64 //本阶段的起始高度
	PreBlockHash      []byte //前一个拉取的BlockHash
	logger            *zap.SugaredLogger
	TransferHeight    uint64
	Pool              *ants.Pool
	signal            chan struct{}
	nextRound         chan struct{}
	ThresholdValue    uint64
}

type SendInfo struct {
	BInfo   *transfer.BlockInfo //这里就是强耦合的
	Message string
	Done    bool
}

func NewFetcher(height uint64, pre []byte, chainId, userName, path string, send chan *provider.SendInfo,
	logger *zap.SugaredLogger) (*Fetcher, error) {

	var TransferHeight uint64
	//init vars
	if configL.TransferConfig != nil {
		RoundNum = configL.TransferConfig.Async.RoundNum
		ThresholdRound = configL.TransferConfig.Async.ThresholdRound
		TransferHeight = configL.TransferConfig.OriginChain.TransferHeight
	}

	fetcher := &Fetcher{
		logger:         logger,
		SendBlockInfo:  send,
		StartHeight:    height,
		CurrentHeight:  height,
		QueryHeight:    height,
		PreBlockHash:   pre,
		TransferHeight: TransferHeight,
		signal:         make(chan struct{}),
		nextRound:      make(chan struct{}),
		ThresholdValue: uint64(RoundNum) + height,
	}
	fetchChainmaker, err := newFetchChainmaker(path)
	if err != nil {
		return nil, err
	}
	fetcher.Fetch = fetchChainmaker

	fetcher.logger.Infof("New fetcher success fetchType: sdk")

	return fetcher, nil
}

//IsFinish judge h whether finish block height
func (p *Fetcher) IsFinish(h uint64) bool {
	if p.TransferHeight > 0 {
		if h >= p.TransferHeight {
			return true
		} else {
			return false
		}
	} else if atomic.CompareAndSwapUint64(&h, p.LatestBlockHeight, h) {
		return true
	}

	return false
}

//UpdateState update state
func (p *Fetcher) UpdateState() {
	p.CurrentHeight++
	p.QueryHeight++
	return
}

func (p *Fetcher) Start(ctx context.Context) error {
	lastHeight, err := p.Fetch.QueryLatestHeight()
	if err != nil {
		return err
	}

	p.logger.Infof("chainmkaer last block height[%d]", lastHeight)

	if p.StartHeight > lastHeight {
		return errors.New("already is latest height")
	}
	if p.TransferHeight > lastHeight {
		return errors.New("transfer height more than latest height")
	}
	if p.TransferHeight > 0 && p.StartHeight > p.TransferHeight {
		return errors.New("start transfer height more than transfer height")
	}
	p.LatestBlockHeight = lastHeight
	go p.fetchLoop(ctx, configL.TransferConfig.Async.QueryCapable)
	return nil
}

//QueryBlockByHeight query block by height
func (p *Fetcher) QueryBlockByHeight(height uint64) (*common.BlockInfo, error) {
	return p.Fetch.FetchBlockByHeight(height)
}

//GetLastBlockHeight query block by height
func (p *Fetcher) GetLastBlockHeight() (uint64, error) {
	return p.LatestBlockHeight, nil
}

//QueryBlockByHeight query block by height
func (p *Fetcher) QueryBlockByHeightByOptions(height uint64) (*common.BlockInfo, error) {
	return p.Fetch.FetchBlockByHeight(height)
}

//Close close chainmaker sdk and release ants pool
func (p *Fetcher) Close() {
	p.logger.Debugf("close fabric sdk and release goRoutine pool")
	p.Fetch.Stop()
	p.Pool.Release()
	close(p.signal)
}

//CloseSdk close chainmaker sdk
func (p *Fetcher) CloseSdk() {
	p.logger.Debugf("close fabric sdk and release goRoutine pool")
	p.Fetch.Stop()
}

//NextRound enter next query round
func (p *Fetcher) NextRound() {
	p.logger.Debugf("receive next round signal")
	p.nextRound <- struct{}{}
}

/*
* 1: 循环拉取fabric链网络的区块
* 2：
 */
func (p *Fetcher) fetchLoop(ctx context.Context, poolCapacity int) {
	p.logger.Debugf("start loop query fabric block module,start height[%d] query height[%d]", p.CurrentHeight, p.QueryHeight)
	var err error
	p.Pool, err = ants.NewPool(poolCapacity, ants.WithPreAlloc(true))
	if err != nil {
		panic("new ants goroutine pool failed,err: " + err.Error())
	}
	signal := make(chan struct{})

	for {
		select {
		case <-ctx.Done():
			p.logger.Infof("close fabric fetch goroutine %s", ctx.Err())
			p.Close()
			return

		case <-signal:
			p.logger.Debugf("recv close signal,wait query block over")
			<-p.signal //wait 全部的query fabric block协程完成，并且下一阶段接受到最后一个block。由消费者管理close
			p.logger.Debugf("recv close signal,close query loop ")
			return

		case <-p.nextRound:
			p.logger.Debugf("enter next query round")
			p.ThresholdValue += uint64(RoundNum)

		default:
			if p.ThresholdValue <= p.CurrentHeight {
				p.logger.Debugf("query fabric block too fast wait 50 ms")
				time.Sleep(50 * time.Millisecond) //sleep and wait consumer
				continue
			}

			p.CurrentHeight++
			if p.CurrentHeight > p.LatestBlockHeight { //fabric 查询到的是区块数量，chainmaker 是latest 区块高度，所以这里不一致
				lastheight, err := p.Fetch.QueryLatestHeight()
				if err != nil { //记录查询
					p.logger.Error(fmt.Errorf("fabric client query channelInfo happen fail,query height is [%d],"+
						"err [%s]", p.CurrentHeight, err))
					time.Sleep(time.Second) //可能是网络问题，休眠一段时间重新链接
					continue
				}

				if p.CurrentHeight > lastheight {
					p.logger.Infof("already query latest fabric block height")
					close(signal)
					continue
				}
				atomic.SwapUint64(&p.LatestBlockHeight, lastheight)
			}

			if p.TransferHeight > 0 && p.CurrentHeight > p.TransferHeight {
				p.logger.Infof("arrive transfer fabric block height[%d]", p.TransferHeight)
				close(signal)
				continue
			}

			p.Pool.Submit(func() { //由于ants内部任务是随机的，所以我们使用原子变量来存储高度
				height := atomic.AddUint64(&p.QueryHeight, 1)
				for {
					startT := time.Now()

					block, err := p.Fetch.FetchBlockByHeight(height)
					if err != nil { //记录查询
						p.logger.Error(fmt.Errorf("origin chain client query block happen fail,query height is [%d],"+
							"err [%s]", height, err))

						time.Sleep(100 * time.Millisecond) //可能是网络问题，休眠一段时间并重新查询
						continue
					}
					p.logger.Debugf("query block height[%d] end spend time[%+v] ", height, time.Since(startT))

					p.SendBlockInfo <- &provider.SendInfo{
						Height: uint64(block.Block.Header.BlockHeight),
						Block:  block,
					}
					return
				}

				//info := fmt.Sprintf("The maximum number of retries exceeded,query block[%d]", height)
				//p.logger.Infof(info)
				//close(signal)
				//bInfo := &SendInfo{
				//	BInfo: nil,
				//	Message: info,
				//}
				//p.SendBlockInfo <- bInfo
			})

		}
	}

}
