package provider

type Transfer interface {
	Start() error
	Stop() error
	IsFinish() bool
	GetStatus()
}
