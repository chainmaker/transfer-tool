/**
 * @Author: jasonruan
 * @Date:   2020-12-09 18:19:36
 **/

package config

import (
	"chainmaker.org/chainmaker/store/v2/conf"
)

type LogConfig struct {
	LogLevelDefault string            `mapstructure:"log_level_default" yaml:"log_level_default"`
	LogLevels       map[string]string `mapstructure:"log_levels" yaml:"log_levels"`
	FilePath        string            `mapstructure:"file_path" yaml:"file_path"`
	MaxAge          int               `mapstructure:"max_age" yaml:"max_age"`
	RotationTime    int               `mapstructure:"rotation_time" yaml:"rotation_time"`
	LogInConsole    bool              `mapstructure:"log_in_console" yaml:"log_in_console"`
	ShowColor       bool              `mapstructure:"show_color" yaml:"show_color"`
}

type DBConfig struct {
	Mysql MysqlConfig `mapstructure:"mysql"`
	Redis RedisConfig `mapstructure:"redis"`
}

type MysqlConfig struct {
	UserName  string `mapstructure:"user_name"`
	Password  string `mapstructure:"password"`
	MysqlIp   string `mapstructure:"mysql_ip"`
	MysqlPort int    `mapstructure:"mysql_port"`
	DataBase  string `mapstructure:"data_base"`
	Key       string `mapstructure:"key"`
}

type RedisConfig struct {
	DB        int    `mapstructure:"db"`
	Password  string `mapstructure:"password"`
	RedisIp   string `mapstructure:"redis_ip"`
	RedisPort int    `mapstructure:"redis_port"`
	Key       string `mapstructure:"key"`
	Auth      string `mapstructure:"Auth"`
}

type AdminUser struct {
	OrgId                string `mapstructure:"org_id" yaml:"org_id"`
	AdminSignKeyFilePath string `mapstructure:"admin_sign_key_file_path" yaml:"admin_sign_key_file_path"`
	AdminSignCrtFilePath string `mapstructure:"admin_sign_crt_file_path" yaml:"admin_sign_crt_file_path"`
}

type ChainClient struct {
	ChainId             string       `mapstructure:"chain_id" yaml:"chain_id"`
	OrgId               string       `mapstructure:"org_id" yaml:"org_id"`
	UserSignKeyFilePath string       `mapstructure:"user_sign_key_file_path" yaml:"user_sign_key_file_path"`
	UserSignCrtFilePath string       `mapstructure:"user_sign_crt_file_path" yaml:"user_sign_crt_file_path"`
	PrivKeyFilePath     string       `mapstructure:"priv_key_file_path" yaml:"priv_key_file_path"`
	CertFilePath        string       `mapstructure:"cert_file_path" yaml:"cert_file_path"`
	HashType            string       `mapstructure:"hash_type" yaml:"hash_type"`
	Genesis             string       `mapstructure:"genesis" yaml:"genesis"`
	AdminUsers          []*AdminUser `mapstructure:"admin_users" yaml:"admin_users"`
	ConsensusId         string       `mapstructure:"consensus_id" yaml:"consensus_id"`
	AuthType            string       `mapstructure:"auth_type" yaml:"auth_type"`
	IsMul               bool         `mapstructure:"is_mul" yaml:"is_mul"`
}

type ContractInfo struct {
	AssistContractName string `mapstructure:"assist_contract_name" yaml:"assist_contract_name"`
	TransferMethod     string `mapstructure:"transfer_method" yaml:"transfer_method"`
	UserContractPath   string `mapstructure:"user_contract_path" yaml:"user_contract_path"`
	AssistContractPath string `mapstructure:"assist_contract_path" yaml:"assist_contract_path"`
}

type OriginChain struct {
	TransferHeight uint64         `mapstructure:"transfer_height" yaml:"transfer_height"`
	ChainType      string         `mapstructure:"chain_type" yaml:"chain_type"`
	ChainVersion   string         `mapstructure:"chain_version" yaml:"chain_version"`
	Fabric         FabricInfo     `mapstructure:"fabric" yaml:"fabric"`
	ChainMaker     ChainMakerInfo `mapstructure:"chain_maker" yaml:"chain_maker"`
}

type FabricInfo struct {
	ChainName  string   `mapstructure:"chain_name" yaml:"chain_name"`
	UserName   string   `mapstructure:"user_name" yaml:"user_name"`
	ConfigPath string   `mapstructure:"config_path" yaml:"config_path"`
	Peers      []string `mapstructure:"peers" yaml:"peers"`
}

type ChainMakerInfo struct {
	ChainName  string `mapstructure:"chain_name" yaml:"chain_name"`
	UserName   string `mapstructure:"user_name" yaml:"user_name"`
	ConfigPath string `mapstructure:"config_path" yaml:"config_path"`
}

type NodeInfo struct {
	NodeId string `mapstructure:"node_id" yaml:"node_id"`
	Ip     string `mapstructure:"ip" yaml:"ip"`
	Port   int    `mapstructure:"port" yaml:"port"`
}

type Async struct {
	QueryCapable   int `mapstructure:"query_capable" yaml:"query_capable"`
	ParseCapable   int `mapstructure:"parse_capable" yaml:"parse_capable"`
	RoundNum       int `mapstructure:"round_num" yaml:"round_num"`
	ThresholdRound int `mapstructure:"threshold_round" yaml:"threshold_round"`
}

type Config struct {
	LogConfig    LogConfig           `mapstructure:"log"`
	Chain        ChainClient         `mapstructure:"chain"`
	ContractInfo ContractInfo        `mapstructure:"contract_info" yaml:"contract_info"`
	OriginChain  OriginChain         `mapstructure:"origin_chain"`
	Node         []*NodeInfo         `mapstructure:"node"`
	Storage      *conf.StorageConfig `mapstructure:"storage"`
	Async        *Async              `mapstructure:"async"`
}
