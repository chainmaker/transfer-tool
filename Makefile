build:
	go build .

lint:
	golangci-lint run ./...

ut:
	go test ./...