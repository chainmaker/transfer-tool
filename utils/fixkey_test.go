/**
 * @Author: starxxliu
 * @Date: 2022/2/17 10:39 上午
 */

package utils

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestFixKey(t *testing.T) {
	key1 := []string{"nftkey", "test1", "test2", "test3"}
	key2 := "key"
	ck := compositeKeyNamespace + key2 + string(rune(minUnicodeRuneValue))
	for _, key := range key1 {
		ck += key + string(rune(minUnicodeRuneValue))
	}
	key, err := FixKey(ck)
	require.Nil(t, err)
	println(key)
}
